﻿TRABALHO DE REDES DE COMPUTADORES - WEBCRAWLER

Integrantes:
	Fernando Angelin
	Gustavo Magalhaes
	Maximiliano Dalla Porta
	Matheus Pinto

Implementação: 
	A implementação do programa foi feita na linguagem Ruby. Esta linguagem é
	interpretada, desta forma não se faz necessário um arquivo Makefile para
	gerar um executável e, nem um script executeme. O programa principal é um
	script chamado main.rb que está dentro da pasta src.
 
	Programas necessarios:
		Ruby 2.0
			-> comando de instalação no MacOS

				port install ruby

		Nokogiri
			-> comandos de instalação no MacOS

				sudo port install libxml2 libxslt

				sudo gem install nokogiri

	Execução:
		Para executar corretamente, entre com o terminal na pasta /src, dentro
		da pasta t1_redes. O comando e os parâmetros sao nessa ordem:

			./main.rb <url> <profundidade>

		onde: url e o link para o site onde irá comecar a busca e profundidade
		e o número do tamanho da profundidade da busca. Ex:

			./main.rb https://www.google.com.br 5

	Testes realizados:
		As páginas são salvas dentro da pasta /pages, que fica dentro da
		pasta t1_redes. Com profundidade 0, será salva apenas a página do
		link passado como parâmetro; com profundidade 1, ele salva além
		dela mesma, as páginas de links nela encontradas; e assim por diante.

		Em alguns servidores é recebido 301, que é uma página de
		redirecionamento para o próprio link visitado.

		Roda perfeitamente com o endere o http://www.google.com.br

		Alguns servidores de sites provavelmente bloqueiam a conexão do socket
		ou não entenda, ou achar que se trata de um ataque.

		Acontece o erro URI.parse() quando o programa encontra urls anomalas,
		como por exemplo: domain/path/query/query, encontradas nos links do Google.