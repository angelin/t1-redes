#!/usr/bin/env ruby

class String
	def to_filename
		return self.split(/[.:\\\/]/).join('_')
	end
end
