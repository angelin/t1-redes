#!/usr/bin/env ruby

require './WebPage'
require 'uri'
require 'socket'

class Connection
	def initialize(url, port = 80, debug = true)
		@port = port
		@uri = URI.parse(url)

		#pega o tipo de conexão HTTP ou HTTPS
		@scheme = @uri.scheme
		
		#pega o nome do servidor em que a página se encontra
		@domain = @uri.host
		
		#pega o local dentro do servidor onde página se encontra
		if @uri.path == ""
			@path = "/"
		elsif @uri.path.match(/\/$/)
			@path = @uri.path.slice!(0..@uri.path.length-2)
		else
			@path = @uri.path
		end

		@query = @uri.query

		@fragment = @uri.fragment
		
		#gera o pedido
		@request = "GET #{@path}"
		@request<< "?#{@query}" if @query
		@request<< "##{@fragment}" if @fragment
		@request<< " HTTP/1.1\r\n"
		
		@request<< "Host: #{@domain}:#{port}\r\n\r\n"
		
		#caso esteja em Debug, escreve o valor do pedido
		if debug
			puts "\n#{@request}"
		end

		begin
			connection = TCPSocket.new(@domain, @port)
		rescue
			puts "Error: #{$!} !!!\n"
		else
			connection.print(@request)

			answer = connection.read

			connection.close

			@header, @body = answer.split("\r\n\r\n", 2)
		end
	end

	# retorna pagina web
	def page
		return WebPage::new(@uri.to_s, @body)
	end
	# retorna o cabeçalho da resposta HTTP
	def header
		return @header
	end
end
